package notiminer.projects.amirahmadadibi.com.cleanadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class ViewHolder extends RecyclerView.ViewHolder {
    public TextView txt_info;
    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_info = itemView.findViewById(R.id.txt_info);
    }
}
