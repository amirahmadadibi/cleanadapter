package notiminer.projects.amirahmadadibi.com.cleanadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import static android.content.ContentValues.TAG;

public class SimpleAdapter extends RecyclerView.Adapter<SimpleViewHolder> {

    List<Product> products = new ArrayList<>();
    Context context;

    public SimpleAdapter(List<Product> products, Context context) {
        this.products = products;
        this.context = context;
    }


    //responsible for inflating view
    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.my_text_view, viewGroup, false);
        SimpleViewHolder simpleViewHolder = new SimpleViewHolder(view);
        Log.d("rv", "onCreateViewHolder: item" + i);
        return simpleViewHolder;
    }

    //every time item added to list
    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder simpleViewHolder, int i) {
        Log.d("rv", "onBindViewHolder: item" + i);
        simpleViewHolder.mTxtInfo.setText(products.get(i).getInfo());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


}
