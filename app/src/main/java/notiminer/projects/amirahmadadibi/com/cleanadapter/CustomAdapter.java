package notiminer.projects.amirahmadadibi.com.cleanadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

abstract class CustomAdapter<T> extends RecyclerView.Adapter {
    List<T> listzz;

    public CustomAdapter(List<T> t) {
        this.listzz = t;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return customOnCreateViewHolder(viewGroup, i);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        customOnBdinViewHolder(viewHolder, i);
    }

    @Override
    public int getItemCount() {
        return listzz.size();
    }

    abstract void customOnBdinViewHolder(RecyclerView.ViewHolder viewHolder, int i);

    abstract RecyclerView.ViewHolder customOnCreateViewHolder(@NonNull ViewGroup viewGroup, int i);
}
