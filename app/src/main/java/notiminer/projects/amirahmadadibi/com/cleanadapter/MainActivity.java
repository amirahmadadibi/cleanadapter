package notiminer.projects.amirahmadadibi.com.cleanadapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rv_info = findViewById(R.id.rv_info);

        Product product1 = new Product("product 1");
        Product product2 = new Product("product 1");
        Product product3 = new Product("product 1");
        Product product4 = new Product("product 1");
        Product product5 = new Product("product 1");
        Product product6 = new Product("product 6");
        Product product7 = new Product("product 7");
        Product product8 = new Product("product 8");
        Product product9 = new Product("product 9");
        Product product10 = new Product("product 10");
        Product product11 = new Product("product 11");
        Product product12 = new Product("product 12");
        Product product13 = new Product("product 13");
        Product product14 = new Product("product 14");
        List<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        products.add(product3);
        products.add(product4);
        products.add(product5);
        products.add(product6);
        products.add(product7);
        products.add(product8);
        products.add(product9);
        products.add(product10);
        products.add(product11);
        products.add(product12);
        products.add(product13);
        products.add(product14);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_info.setLayoutManager(mLayoutManager);

//        SimpleAdapter myAdapter = new SimpleAdapter(products,MainActivity.this);
//        rv_info.setAdapter(myAdapter);

        MyAdapter myAdapter = new MyAdapter(products);
        rv_info.setAdapter(myAdapter);
    }
}
