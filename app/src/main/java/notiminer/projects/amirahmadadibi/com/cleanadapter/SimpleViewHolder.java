package notiminer.projects.amirahmadadibi.com.cleanadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

//first - crate ViewHolder class
public class SimpleViewHolder extends RecyclerView.ViewHolder {
    TextView mTxtInfo;

    public SimpleViewHolder(@NonNull View itemView) {
        super(itemView);
        mTxtInfo = itemView.findViewById(R.id.txt_info);
    }
}
