package notiminer.projects.amirahmadadibi.com.cleanadapter;

public class Product {
    String info;

    public Product(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
