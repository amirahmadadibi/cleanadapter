package notiminer.projects.amirahmadadibi.com.cleanadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends CustomAdapter<> {
    SimpleViewHolder simpleViewHolder;
    List<Product> products = new ArrayList<>();

    public MyAdapter(List<Product> t) {
        super(t);
        products = t;
    }

    @Override
    void customOnBdinViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        simpleViewHolder.mTxtInfo.setText(products.get(i).getInfo());
    }

    @Override
    RecyclerView.ViewHolder customOnCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.my_text_view, viewGroup, false);
        simpleViewHolder = new SimpleViewHolder(view);
        return simpleViewHolder;
    }
}
